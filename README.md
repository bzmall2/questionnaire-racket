
Example Racket `scribble\lp2` and `scribble\eval` pages.

For now this is just an exploration of gitlab Pages with a project I hope to keep working on.

If I can learn to work with gitlab pages along with `scribble\lp2` I hope to share Racket scripts that reform and review faculty development questionnaire data.


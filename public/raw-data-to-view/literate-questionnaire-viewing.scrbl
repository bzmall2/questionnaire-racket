#lang scribble/lp2

@(require scribble/manual scribble/eval scriblib/footnote scriblib/autobib racket/list racket/match csv-reading)

@(define data-dir "data")
@(match-define (list  Q-info         R-info      results-file)
               (list "SetsuMon.csv" "KaiTou.csv" "data.csv"))
@(define (get-file-into-lists data-dir csv-name)
  (define data-reader
    (make-csv-reader	
     (open-input-file
      (build-path data-dir csv-name))))
  (csv->list data-reader))
	       
@(define results (get-file-into-lists data-dir results-file))
@(define Q-info-lists (get-file-into-lists data-dir Q-info))
@(define R-info-lists (get-file-into-lists data-dir R-info))

@(require scribble/core
	  scribble/html-properties)
@(define too-wide
   (make-style "img-too-wide"
              (list (make-css-addition "img-too-wide.css"))))
@(define my-img-dir "images")
@(define (my-img file-name #:style (style ""))
  (image (build-path my-img-dir file-name) #:style style))

@(define qv-eval (make-base-eval))
@(void (interaction-eval #:eval qv-eval
                         (require racket/string racket/list
			 csv-reading racket/path plot plot/utils
			 racket/match 
			 (rename-in pict
			   (table pict-table)
			   (frame pict-frame)))))
			 
@(define-syntax-rule (qv-interaction e ...)
   (interaction #:eval qv-eval e ...))


@title[#:style (list 'no-toc 'no-sidebar)]{View Questionnaire Results with Racket}

Racket's @racket[scribble/lp2] and @racket[scribble/eval] will let us write our way through a process to visualize quesionnaire data. All the text needed to generate plots and pages to for viewing will be in a @code{data} directory. One class's questionnaire response are in the @code{responses.csv} file.

@(chunk <directory-file-setup>
  (define data-dir "data")
  (define responses-file "responses.csv")
  (define response-results (build-path data-dir responses-file)))

We can see tests of our @italic{Literate Programming} @racket[chunk]s with @racket[scribble/eval].

@(qv-interaction
  (define data-dir "data")
  (define results-file "responses.csv")
  (define response-results (build-path data-dir results-file))
  (display (path->string response-results))
  )

But I had to get the responses from printed questionnaire pages before I could get started forming and viewing the data.

After collecting questionnaire sheets from a class it can be reassuring to use @italic{simple scan} and a small scanner to get the sheets on your screen. Numbering the sheets lets you make sure the scanner's feeder mechanism handled every page well.

@(my-img "PC-gnu-sheets-snapscan-edt.jpg" #:style too-wide)

I use @italic{emacs} with @italic{org-mode} for data-entry. When there are more than 10 or 20 sheets for data-entry it's good to have the questions and responses on-screen next to the @italic{org-mode} table.

@(my-img "evince-pdf-org-table-data-entry-start-1.png" #:style too-wide)

They pdf viewer @italic{evince} seems to come with a typical installation of the @italic{GNU/Linux} Operating System's @italic{Debian} distribution  with the @italic{Gnome} Desktop Environment. @italic{Ubuntu} distributions seem to provide @italic{evince} too. Scrolling @italic{evince} while typing data into @italic{org-mode} worked well for me.

@(my-img "evince-scrolls-scan-page-numbering-checks-org-entry.png" #:style too-wide)

With @italic{emacs} in @italic{org-mode} you have all the conveniences of a spreadsheet table while working with all text. Text files are much lighter than office spreadsheet files, and text files are convenient for @code{diff} and Version Control Systems like @italic{git}, @italic{bazaar}, and @italic{mercury, hg}.

@(my-img "org-table-export.png") @; #:style too-wide

The @italic{org-mode} table is quickly exported to @italic{.csv} file of Comma Separated Values. The whole @italic{org-mode} document can be exported to a @italic{html} page for the browser. It can be good to look over a document in another form, maybe typos and other errors are more noticeable with a different presentation.

Once we have to data in a minimal @italic{csv} file @racket[csv-reading] lets us get values into racket in a compact way.

@(chunk <csv-read-results>
(require csv-reading racket/list)
(define responses
  (make-csv-reader
    (open-input-file response-results)))
(define response-lists (csv->list responses))

(code:comment @#,elem{(display (first response-lists))})
)

Let's do a quick check of the @racket[chunk] above with an @racket[interaction].

@(qv-interaction
(require csv-reading)
(define responses
  (make-csv-reader
    (open-input-file response-results)))
(define response-lists (csv->list responses))

(list (first response-lists) (second response-lists))
)

We have the raw-data for responses ready for use with Racket, but the data is in a shortened format. For viewing we will make it easy to visually associate the questions with the number of positive and negative responses.

The short-name, question-type, and text for each question are, like the @code{response-results}, in a @code{.csv}-format @italic{file} in the @code{data} @italic{directory}. We will be getting @code{.csv} data into Racket lists two more times just to view the response from one class. Later we'll want to view together for comparison the responses from a few different classes, or view the response frequencies of an entire department or school. The repetition in the work suggests a need for a procedure that takes a file-name and returns the file's contents as lists.

@(qv-interaction
  (define (get-file-into-lists csv-dir csv-name)
    (define data-reader
      (make-csv-reader	
        (open-input-file (build-path csv-dir csv-name))))
    (csv->list data-reader))
  (define Qs-file "SetsuMon.csv")
  (get-file-into-lists data-dir Qs-file))

Now that a @racket[scribble/eval] @racket[interaction] has shown that the procedure works we can put it in a @racket[scribble/lp2] @racket[chunk] along with the names of the @code{.csv} files in the @code{data} @italic{directory}.

@(chunk <get-csv-data-into-lists>
  (require racket/match)
  (match-define (list  Q-info         R-info)
                (list "SetsuMon.csv" "KaiTou.csv"))
    (define (get-file-into-lists data-dir csv-name)
      (define data-reader
        (make-csv-reader	
          (open-input-file
	    (build-path data-dir csv-name))))
     (csv->list data-reader))
  (define Q-info-lists (get-file-into-lists data-dir Q-info))
  (define R-info-lists (get-file-into-lists data-dir R-info))
  (code:comment @#,elem{R-info-lists})
  )

Before commenting @racket{;; R-info-lists} out this document produced lists response information.

@(my-img "terminal-FF-data-into-lists.png")

Let's assume a convention for the format of the @code{.csv} files:  the first and second lines are @italic{meta-data} about the class.
	
@(qv-interaction 
  (take response-lists 2))

The convention-following @code{.csv} file's  third line lists @italic{headers} that could serve as the first @italic{head}er row of a @italic{table} whose contents are the remaining rows. Viewing the short @italic{meta-data} rows above is compact, but viewing long result-data rows is unwieldy.

@(my-img "FF-emacs-interaction-output-too-long.png")

@(chunk <tabular-results-sample-just-for-show>
  (tabular #:sep (hspace 1)
           (take (drop results 2) 3)))
@note{The results used to develop this programming example is from a second-semester English class for Japanese students in Japan. These students had failed the first semester's class were taking the class again. As a teacher it is invigorating to see a comment such as "I came to understand things I did not understand during the first semester."}

@(tabular #:sep (hspace 1)
          (take (drop results 2) 3))

Let's see the  @italic{responses information} data as a table.

@(chunk <tabular-responses-just-for-show>
  (tabular #:sep (hspace 1)
          R-info-lists))

@(tabular #:sep (hspace 1)
          R-info-lists)

@; inspired by rebellion/collecion/multisetl
@; ; mulitset-frequency

I already have code to plot views of frequency data. So now I'm thinking about taking the responses @code{.csv} data and writing to a frequency @code{.csv} file that follows the same conventions.

@(my-img "raw-data-to-freq-data-gedit.png" #:style too-wide)
@; for readability, style fit-to-narrower-container

The typical questionnaire at this school has 11 questions. I sometimes have two more questions for my classes but not many students notice the extras. But the additional questions can be good for eliciting comments from the students that doo notice them.

The raw data has a column for each question and a rows each questionnaire sheet. To see frequencies I want a column for each response type and a row for each question. I have to @code{reduce} the data for each question to frequencies. 

Later I'll produce a page with comments and the number responses that were on the same sheet. People that bother to write comments may have given the class or the questions some thought while writing on the form.

But for now I want to get the data into a form that works with the plot code.

@(qv-interaction
  (define 1-list '(1 3 4 1 1 1 3 3))
  (define a-list (map number->string 1-list))
  a-list
  (define (get-count elem lst)
  (count (lambda (e) (equal? e elem))
	 lst))
  (get-count "3" a-list)
  (get-count "4" a-list)
  1-list
  (get-count 3  1-list))

@(chunk <get-count-definition>
  (define (get-count elem lst)
  (count (lambda (e) (equal? e elem))
        lst))
)


@(chunk <*>
  <directory-file-setup>
  <csv-read-results>
  <get-csv-data-into-lists>

  <get-count-definition>  
)

#+TITLE: Questionnaire Racket Index Page
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>


** Getting Started with Raw Questionnaire Data

  - [[file:raw-data-to-view/literate-questionnaire-viewing.html][Literate Questionaire Viewing]]


I'm hoping that I can share scribble documents by just pushing all the scribble ouput to the `public` folder of a gitlab project with the `.gitlab-ci.yml` file for a Plain HTML site.

The code `.scrbl` code itself is broken: the `my-img` procedure does not seem to work as intended. But, for a test of Pages this is the best repo I have right now.

Discovering the basic settings for `scribble\lp2` and `scribble\eval` took a lot of time, so that part of the code might be useful for later reference. 
